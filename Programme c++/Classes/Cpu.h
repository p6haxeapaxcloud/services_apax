#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#ifndef _CPU_H
#define _CPU_H

class Cpu
{
	private:
		float cpuloadavg;
		float stat[5];
		
	public:
		Cpu();
		~Cpu();
		float GetCpuValues();	
};


#endif
