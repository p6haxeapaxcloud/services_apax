#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#ifndef _RAM_H
#define _RAM_H


class Ram
{
private:
	float ramloadavg;
	float free;
	float memfree;
	float total;
public:
		Ram();
		~Ram();
		float GetRamValues();		
};

#endif
