#include "CpuTest.h"

/*Cpu::Cpu() {

}

Cpu::~Cpu() {

}*/

float Cpu::GetCpuValuesTest() {
	FILE *procStat = fopen("/proc/stat", "r"); //Ouverture du fichier /proc/stat en lecture.
    fscanf(procStat," %*s %f %f %f %f %f",&stat[0],&stat[1],&stat[2],&stat[3],&stat[4]); //Affectation des 5 premières valeurs dans un tableau.
    fclose(procStat);
    cpuloadavg = (((stat[0]+stat[1]+stat[2]+stat[3]+stat[4])-(stat[3]+stat[4]))/(stat[0]+stat[1]+stat[2]+stat[3]+stat[4]))*100; //Calcule du pourcentage CPU utilisé à partir du fichier /proc/stat
    printf("Total: %f\n",stat[0]+stat[1]+stat[2]+stat[3]+stat[4]);
    printf("Libre: %f\n",stat[3]+stat[4]);
    printf("Utilisation: %f\n",cpuloadavg); //Affichage de la valeur.
    return cpuloadavg;
}

int main()
{
	Cpu *cpu_ = new Cpu();
    return cpu_->GetCpuValuesTest();
}
