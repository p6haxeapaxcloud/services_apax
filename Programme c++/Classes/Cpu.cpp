#include "Cpu.h"

Cpu::Cpu() {

}

Cpu::~Cpu() {

}

float Cpu::GetCpuValues() {
	FILE *procStat = fopen("/proc/stat", "r"); //Ouverture du fichier /proc/stat en lecture.
    fscanf(procStat," %*s %f %f %f %f %f",&stat[0],&stat[1],&stat[2],&stat[3],&stat[4]); //Affectation des 5 premières valeurs dans un tableau.
    fclose(procStat); //Fermeture du fichier.
    cpuloadavg = (((stat[0]+stat[1]+stat[2]+stat[3]+stat[4])-(stat[3]+stat[4]))/(stat[0]+stat[1]+stat[2]+stat[3]+stat[4]))*100; //Calcule du pourcentage CPU utilisé à partir du fichier /proc/stat
	return cpuloadavg;
}
