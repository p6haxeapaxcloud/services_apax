#include "Gestionnaire.h"

using namespace std;



Gestionnaire::Gestionnaire()
{
	cpu_ = new Cpu();
	ram_ = new Ram();
	ping_ = new Ping();
}

Gestionnaire::~Gestionnaire()
{
	delete cpu_;
	delete ram_;
	delete ping_;
}


void Gestionnaire::Envoie()
{
	pingValues = ping_->GetPingValues();
	ramValues = ram_->GetRamValues();
	cpuValues = cpu_->GetCpuValues();
	//récupération des valeurs

	sock = socket(AF_INET , SOCK_STREAM , 0);
	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons( 8888 );
	//Pré connexion à l'API client

	sprintf(Values, "%f+%f+%f",cpuValues,ramValues,pingValues);
	//Convertion des données pour l'envoie et ajout du signe + entre chaque valeur pour le parsage


  	while(connect(sock , (struct sockaddr *)&server , sizeof(server)) == -1){
		sleep(10);
  	}

	while(write(sock,Values,strlen(Values)) == -0){
		sleep(10);
	}
	//Envoi des données vers l'API client.

        close(sock);
        //deconnexion du serveur tcp
}

int main()
{
	Gestionnaire* gestionnaire = new Gestionnaire();
	while(1){
		gestionnaire->Envoie(); //Appelle de la méthode envoie.
		sleep(1800); //Pause de 30 minutes.
	}
}
