#include "Cpu.h"
#include "Ram.h"
#include "Ping.h"
#include <iostream>
#include <stdio.h> //printf
#include <string.h>    //strlen
#include <sys/socket.h>    //socket
#include <arpa/inet.h> //inet_addr
#include <sstream>
#include <fstream>

#ifndef _GESTIONNAIRETEST_H
#define _GESTIONNAIRETEST_H

class Gestionnaire
{
public:


Gestionnaire();
~Gestionnaire();
void Envoie();

private:

int sock;
struct sockaddr_in server;
float pingValues;
float ramValues;
float cpuValues;
char Values[100];
Cpu *cpu_;
Ram *ram_;
Ping *ping_;

protected:

};


#endif
