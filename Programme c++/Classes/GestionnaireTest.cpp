#include "GestionnaireTest.h"

using namespace std;



Gestionnaire::Gestionnaire()
{
	cpu_ = new Cpu();
	ram_ = new Ram();
	ping_ = new Ping();
}

Gestionnaire::~Gestionnaire()
{
	delete cpu_;
	delete ram_;
	delete ping_;
}


void Gestionnaire::Envoie()
{
	pingValues = ping_->GetPingValues();
	ramValues = ram_->GetRamValues();
	cpuValues = cpu_->GetCpuValues();
	//Récupération des valeurs
    	printf ("Cpu: %f\n",cpuValues);
    	printf ("Ram: %f\n",ramValues);
    	printf ("Ping: %f\n",pingValues);
	//Affichage des statistiques produites


	sock = socket(AF_INET , SOCK_STREAM , 0);
	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons( 8888 );
	//Pré connexion à l'API client
	

	sprintf(Values, "%f+%f+%f",cpuValues,ramValues,pingValues);
	//Conversion des données pour l'envoi et ajout du signe + entre chaque valeur pour le parsage


  	while(connect(sock , (struct sockaddr *)&server , sizeof(server)) == -1){
  		printf("connexion échoué tentative de reconnexion dans 10 secondes... \n");
		sleep(10);
  	}
  	// Si la connexion à l'API client échoue une tentative de reconnexion est effectuée toutes les 10 secondes.
	printf("Connexion réussie envoi des données.... \n");
  	
	while(write(sock,Values,strlen(Values)) == -0){
		printf("Échec de l'envoi tentative d'envoi dans 10 secondes... \n");
		sleep(10);
	}
	
	printf("Envoi réussi !\n");
	printf("Envoyé: %s\n", Values);
	//Envoi des données vers l'API client.

	close(sock);
	//Déconnexion du serveur tcp

}


int main()
{
	Gestionnaire* gestionnaire = new Gestionnaire();
	while(1){
		gestionnaire->Envoie(); //Appelle de la méthode envoie.
		sleep(40); //Pause de 30 secondes.
	}
}
