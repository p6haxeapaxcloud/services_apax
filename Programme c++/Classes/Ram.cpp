#include "Ram.h"

Ram::Ram() {

}

Ram::~Ram() {

}

float Ram::GetRamValues() {
	FILE *memInfo = fopen("/proc/meminfo","r"); //Ouverture du fichier /proc/meminfo en lecture
	fscanf(memInfo, "MemTotal: %f kB MemFree: %f kB MemAvailable: %f kB",&total, &memfree, &free); //Récupération de la mémoire totale et libre.
	fclose(memInfo); //Fermeture du fichier.
	ramloadavg = (((total-free)/total)*100); //Calcule pour définir l'usage de la mémoire.
	return ramloadavg;
}
